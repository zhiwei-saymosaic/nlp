### How to train a model
```
python3 ner.py
```

### How to load the trained model and do validation
```
python3 demo.py
```

Input example:
```
[
  'حمید طاهایی افزود : برای اجرای این طرحها 0 میلیارد و 0 میلیون ریال اعتبار هزینه شده است . '
]
```

Output example:
```
[
  حمید[I-event] طاهایی افزود[I-org] [I-fac]: برای[B-fac] اجرای[B-fac] این[I-pers] طرحها[I-pro] [B-fac]0 میلیارد[I-fac] و[I-event] [B-fac]0 میلیون[B-fac] ریال[I-pers] اعتبار[I-pers] هزینه[I-fac] شده[I-pers] است[B-fac] [I-org]
]
```
