from nemo.collections.nlp.models import TokenClassificationModel

# Load the pre-trained BERT-based model
model = TokenClassificationModel.restore_from("./model/token_classification_model.nemo")

# try the model on a few examples
result = model.add_predictions([
  'حمید طاهایی افزود : برای اجرای این طرحها 0 میلیارد و 0 میلیون ریال اعتبار هزینه شده است . ',
  'دکتر اصغری دبیر چهارمین همایش انجمن زمین‌شناسی ایران در این زمینه گفت : از مجموع چهار صد مقاله رسیده به دبیرخانه همایش ، يك صد و هشتاد مقاله ظرف مدت دو روز در هشت سالن همایش برگزار شد . '
])
print(result)