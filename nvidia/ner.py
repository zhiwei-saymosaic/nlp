from nemo.collections import nlp as nemo_nlp
from nemo.utils.exp_manager import exp_manager

import os
import wget 
import torch
import pytorch_lightning as pl
from omegaconf import OmegaConf




WORK_DIR = "./"
MODEL_CONFIG = "token_classification_config.yaml"
config_dir = WORK_DIR + 'configs/'
os.makedirs(config_dir, exist_ok=True)


# download the model's configuration file 
if not os.path.exists(config_dir + MODEL_CONFIG):
    print('Downloading config file...')
    wget.download(f'https://raw.githubusercontent.com/NVIDIA/NeMo/master/examples/nlp/token_classification/conf/' + MODEL_CONFIG, config_dir)
else:
    print ('config file is already exists')


# load configuration file
config_path = f'{WORK_DIR}/configs/{MODEL_CONFIG}'
config = OmegaConf.load(config_path)

# modify configuration
DATA_DIR = "./data-sample"
config.model.dataset.data_dir = DATA_DIR
# if you want to use the full dataset, set NUM_SAMPLES to -1
NUM_SAMPLES = -1
config.model.train_ds.num_samples = NUM_SAMPLES
config.model.validation_ds.num_samples = NUM_SAMPLES
config.trainer.max_epochs = 5

# build trainer
# checks if we have GPU available and uses it
cuda = 1 if torch.cuda.is_available() else 0
config.trainer.gpus = cuda
config.trainer.precision = 16 if torch.cuda.is_available() else 32
# remove distributed training flags
config.trainer.accelerator = None
# setup max number of steps to reduce training time for demonstration purposes of this tutorial
# config.trainer.max_steps = 32
trainer = pl.Trainer(**config.trainer)


# exp_dir = exp_manager(trainer, config.get("exp_manager", None))

# set base model
# get the list of supported BERT-like models, for the complete list of HugginFace models, see https://huggingface.co/models
# print(nemo_nlp.modules.get_pretrained_lm_models_list(include_external=True))
# specify BERT-like model, you want to use
PRETRAINED_BERT_MODEL = "bert-base-multilingual-uncased"
config.model.language_model.pretrained_model_name = PRETRAINED_BERT_MODEL
model = nemo_nlp.models.TokenClassificationModel(cfg=config.model, trainer=trainer)


queries = [
  'حمید طاهایی افزود : برای اجرای این طرحها 0 میلیارد و 0 میلیون ریال اعتبار هزینه شده است . ',
  'دکتر اصغری دبیر چهارمین همایش انجمن زمین‌شناسی ایران در این زمینه گفت : از مجموع چهار صد مقاله رسیده به دبیرخانه همایش ، يك صد و هشتاد مقاله ظرف مدت دو روز در هشت سالن همایش برگزار شد . '
]
results = model.add_predictions(queries)
for query, result in zip(queries, results):
    print()
    print(f'Query : {query}')
    print(f'Result: {result.strip()}\n')


print("Trainer config - \n")
print(OmegaConf.to_yaml(config.trainer))

# start model training
trainer.fit(model)

model.save_to('./model/token_classification_model.nemo')

results = model.add_predictions(queries)
for query, result in zip(queries, results):
    print()
    print(f'Query : {query}')
    print(f'Result: {result.strip()}\n')

