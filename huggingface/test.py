from transformers import AutoTokenizer, AutoModelForTokenClassification, pipeline


model = AutoModelForTokenClassification.from_pretrained('./model')
tokenizer = AutoTokenizer.from_pretrained('./model')


ner = pipeline('ner', model=model, tokenizer=tokenizer, grouped_entities=True)
result = ner("Elon Reeve Musk FRS is an entrepreneur and business magnate. He is the founder, CEO, and Chief Engineer at SpaceX; early stage investor, CEO, and Product Architect of Tesla, Inc.; founder of The Boring Company; and co-founder of Neuralink and OpenAI.")

print(result)
