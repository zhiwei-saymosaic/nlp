### How to train a model
```
python3 train.py \
  --overwrite_output_dir \
  --model_name_or_path bert-base-uncased \
  --train_file data/train.json \
  --validation_file data/test.json \
  --test_file data/validation.json \
  --output_dir model \
  --do_train \
  --do_eval \
  --do_predict
```

### How to load the trained model and do validation
```
python3 test.py
```

Input example:
```
Elon Reeve Musk FRS is an entrepreneur and business magnate. He is the founder, CEO, and Chief Engineer at SpaceX; early stage investor, CEO, and Product Architect of Tesla, Inc.; founder of The Boring Company; and co-founder of Neuralink and OpenAI.
```

Output example, the result is not good, because the *training* data is too few (Only 5 items):
```
[
  {
    "entity_group": "KEYWORD",
    "score": 0.3927343,
    "word": "is",
    "start": 20,
    "end": 22
  },
  {
    "entity_group": "KEYWORD",
    "score": 0.42242357,
    "word": "entrepreneur",
    "start": 26,
    "end": 38
  },
  {
    "entity_group": "KEYWORD",
    "score": 0.41369453,
    "word": "magnate",
    "start": 52,
    "end": 59
  },
  {
    "entity_group": "KEYWORD",
    "score": 0.41518533,
    "word": "early",
    "start": 115,
    "end": 120
  },
  {
    "entity_group": "KEYWORD",
    "score": 0.40264136,
    "word": "architect of",
    "start": 154,
    "end": 166
  },
  {
    "entity_group": "KEYWORD",
    "score": 0.40927428,
    "word": "founder of",
    "start": 180,
    "end": 190
  },
  {
    "entity_group": "KEYWORD",
    "score": 0.3825849,
    "word": "open",
    "start": 243,
    "end": 247
  }
]
```